#!/usr/bin/env groovy

/*
  Copyright (C) 2017 Collabora Limited
  Author: Guillaume Tucker <guillaume.tucker@collabora.com>

  This module is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
  details.

  You should have received a copy of the GNU Lesser General Public License
  along with this library; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/* Note: docker_image.inside() needs to be called after dir(some_directory)
 * otherwise it fails and stays at the root of the workspace. */

def DISTRO = "stretch"
def VARIANT = "minbase"
def MIRROR = "http://ftp.uk.debian.org/debian/"
def REGISTRY = "docker-registry.internal.collabora.com"

node("docker-slave") {
    docker.withRegistry("https://${REGISTRY}",
                        "ccu-docker-internal-collabora-com") {
        def docker_image = null
        def out_dir_top = env.WORKSPACE + '/output/debian'
        def out_dir = out_dir_top + "/${currentBuild.number}"

        stage("job checkout") {
            dir("checkout") {
                git(url: env.gitlabSourceRepoHttpUrl,
                    branch: env.gitlabBranch,
                    poll: false)
            }
        }

        stage("builder image") {
            dir("checkout") {
                docker_image = docker.build(
                    "${REGISTRY}/reference-build/debootstrap",
                    "docker")
                docker_image.push()
            }

            dir(out_dir) {
                def params = """\
distro:   ${DISTRO}
variant:  ${VARIANT}
mirror:   ${MIRROR}
"""
                sh(script: 'echo "' + "${params}" + '" > manifest.txt')
            }
        }

        stage("debootstrap (amd64)") {
            def arch = "amd64"
            def build_dir = env.WORKSPACE + "/rootfs-${arch}"
            docker_image.inside("-u root --privileged") {
                sh(script: """
rm -rf ${build_dir}
mkdir -p ${build_dir}
debootstrap \
  --arch=${arch} \
  --variant=${VARIANT} \
  --include=""" + '"bmap-tools"' + """ \
  ${DISTRO} \
  ${build_dir} \
  ${MIRROR}

tar -C ${build_dir} -czf ${out_dir}/debian-${DISTRO}-${arch}.tar.gz .
""")
            }
        }

        stage("debootstrap (arm64)") {
            def arch = "arm64"
            def build_dir = env.WORKSPACE + "/rootfs-${arch}"
            sh(script: "mkdir -p ${build_dir}")
            docker_image.inside("-u root --privileged") {
                sh(script: """
qemu-debootstrap \
  --arch=${arch} \
  --variant=${VARIANT} \
  --include=""" + '"bmap-tools"' + """ \
  ${DISTRO} \
  ${build_dir} \
  ${MIRROR}

tar -C ${build_dir} -czf ${out_dir}/debian-${DISTRO}-${arch}.tar.gz .
""")
//  || cat /scratch/tmp/jenkins/workspace/Collabora/QA/debian/stretch-x86_64/debootstrap/debootstrap.log
            }
        }

        stage("debootstrap (armhf)") {
            def arch = "armhf"
            def build_dir = env.WORKSPACE + "/rootfs-${arch}"
            sh(script: "mkdir -p ${build_dir}")
            docker_image.inside("-u root --privileged") {
                sh(script: """
qemu-debootstrap \
  --arch=${arch} \
  --variant=${VARIANT} \
  --include=""" + '"bmap-tools"' + """ \
  ${DISTRO} \
  ${build_dir} \
  ${MIRROR}

tar -C ${build_dir} -czf ${out_dir}/debian-${DISTRO}-${arch}.tar.gz .
""")
            }
        }

        stage("upload") {
            dir(out_dir_top) {
                docker_image.inside() {
                    env.NSS_WRAPPER_PASSWD = '/tmp/passwd'
                    env.NSS_WRAPPER_GROUP = '/dev/null'
                    sshagent (credentials: ["images.ccu-upload",]) {
                        sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
                        sh(script: 'LD_PRELOAD=libnss_wrapper.so ssh -oStrictHostKeyChecking=no jenkins-upload@images.collabora.co.uk "mkdir -p /srv/images.collabora.co.uk/www/images/singularity/reference/debian"')
                        sh(script: 'LD_PRELOAD=libnss_wrapper.so rsync -e "ssh -oStrictHostKeyChecking=no" -av --dry-run ' + "${currentBuild.number}" + ' jenkins-upload@images.collabora.co.uk:/srv/images.collabora.co.uk/www/images/singularity/reference/debian/')
                        sh(script: "rm -rf ${currentBuild.number}")
                    }
                }
            }
        }
    }
}
